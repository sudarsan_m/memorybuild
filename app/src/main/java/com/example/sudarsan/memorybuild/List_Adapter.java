package com.example.sudarsan.memorybuild;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by SUDARSAN on 07-03-2018.
 */

public class List_Adapter extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Detail> details;

    public List_Adapter(Context context, int layout, ArrayList<Detail> details) {
        this.context = context;
        this.layout = layout;
        this.details = details;
    }

    @Override
    public int getCount() {
        return details.size();
    }

    @Override
    public Object getItem(int position) {
        return details.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {

        ImageView imageView;
        TextView txtname;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View row = convertView;

        ViewHolder holder = new ViewHolder();

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);


            holder.txtname = (TextView) row.findViewById(R.id.imageView4);
            holder.imageView = (ImageView) row.findViewById(R.id.textView2);
            row.setTag(holder);

        } else {
            holder = (ViewHolder) row.getTag();
        }

        Detail detail = details.get(position);

        holder.txtname.setText(detail.getFname());

        byte[] foodImage = detail.getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
        holder.imageView.setImageBitmap(bitmap);

        return  row;
    }
}
