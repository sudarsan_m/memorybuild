package com.example.sudarsan.memorybuild;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class list_details extends AppCompatActivity {

    TextView e1,e2,e3,e4;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_details);


        e1 = (TextView) findViewById(R.id.fn);
        e2 = (TextView) findViewById(R.id.ln);
        e3 = (TextView) findViewById(R.id.ag);
        e4 = (TextView) findViewById(R.id.ph);
        image = (ImageView)findViewById(R.id.imageView2);


        Cursor cursor = Mydetails.sqLiteHelper.getData("SELECT * FROM PATIENTS");



        try {



            while (cursor.moveToNext()) {

                byte [] byteArray = cursor.getBlob(5);
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);


                e1.setText(cursor.getString(1));
                e2.setText(cursor.getString(2));
                e3.setText(cursor.getString(3));
                e4.setText(cursor.getString(4));
                image.setImageBitmap(Bitmap.createScaledBitmap(bmp, 100,
                        70, false));

            }


        }

        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
        }
    }
}
