package com.example.sudarsan.memorybuild;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.BitSet;

public class Mydetails extends AppCompatActivity {

    EditText fname,lname,age,phone;
    Button save,choose,view;
    ImageView image;

    public static SQLiteHelper sqLiteHelper;
    final int REQUEST_CODE_GALLERY = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mydetails);

        init();

        sqLiteHelper = new SQLiteHelper(this,"PATIENTDB.sqlite",null,1) ;
        sqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS PATIENTS(ID INTEGER PRIMARY KEY AUTOINCREMENT,fname VARCHAR,lname VARCHAR,age VARCHAR,phone VARCHAR,image BLOB)");



        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActivityCompat.requestPermissions(

                        Mydetails.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY

                );

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    sqLiteHelper.InsertData(

                            fname.getText().toString().trim(),
                            lname.getText().toString().trim(),
                            age.getText().toString().trim(),
                            phone.getText().toString().trim(),
                            imageViewToByte(image)
                    );

                    Toast.makeText(getApplicationContext(),"Ssved Successfully",Toast.LENGTH_SHORT).show();
                    fname.setText("");
                    lname.setText("");
                    age.setText("");
                    phone.setText("");
                    image.setImageResource(R.mipmap.ic_launcher);

                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"failed",Toast.LENGTH_SHORT).show();
                }
            }
        });


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Mydetails.this,list_details.class);
                startActivity(intent);
            }
        });


    };

    private byte[] imageViewToByte(ImageView image){

        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] byteArray =  stream.toByteArray();
        return byteArray;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode==REQUEST_CODE_GALLERY){

            if(grantResults.length>0&&grantResults[0]== PackageManager.PERMISSION_GRANTED){

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,REQUEST_CODE_GALLERY);
            }

            else {

                Toast.makeText(getApplicationContext(),"You dont have permission to access",Toast.LENGTH_SHORT).show();
            }

            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==REQUEST_CODE_GALLERY&&resultCode==RESULT_OK&&data!=null){

            Uri uri = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                image.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void init(){

        fname = (EditText)findViewById(R.id.editText2);
        lname = (EditText)findViewById(R.id.editText3);
        age = (EditText)findViewById(R.id.editText4);
        phone = (EditText)findViewById(R.id.editText5);
        save = (Button)findViewById(R.id.button3);
        choose = (Button)findViewById(R.id.button2);
        view = (Button)findViewById(R.id.button8);
        image = (ImageView)findViewById(R.id.imageView);
    }
}
