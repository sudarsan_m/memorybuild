package com.example.sudarsan.memorybuild;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static android.R.attr.data;
import static android.R.id.content;


public class Main4Activity extends AppCompatActivity implements OnItemSelectedListener {


    List<String> places = new ArrayList<String>();
    int selected;
    Spinner spinner;
    TextView tv;
    String data;
    Button b1;
    private String file = "",file2="",file3="",file4="",file5="";
    File f1,f2,f3,f4,f5;
    public static SQLiteHelper sqLiteHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        selected = getIntent().getExtras().getInt("item");


        sqLiteHelper = new SQLiteHelper(this,"PATIENTDB.sqlite",null,1) ;
        sqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS LOCATION(PLACE VARCHAR)");

        switch (selected){

            case 1:
                places.add("A");
                places.add("B");
                places.add("C");
                select(places,file,data);
                break;

            case 2:
                places.add("D");
                places.add("E");
                places.add("F");
                select(places,file2,data);
                break;

            case 3:
                places.add("G");
                places.add("H");
                places.add("I");
                select(places,file3,data);
                break;


            case 4:
                places.add("J");
                places.add("K");
                places.add("L");
                select(places,file4,data);
                break;


            case 5:
                places.add("M");
                places.add("N");
                places.add("O");
                select(places,file5,data);
                break;

        }
    }

    private void select(List<String> places,String file,String data){

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, places);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        //writeToFile(data,file);
       // readFromFile(file);



    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        final String item = parent.getItemAtPosition(position).toString();
        tv = (TextView)findViewById(R.id.textView);
        tv.setText("Item last found on :"+item);
        b1 = (Button)findViewById(R.id.button);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                write(item);

            }
        });
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }


    public void write(String place){

     try{
         sqLiteHelper.ins(place);
         Toast.makeText(getBaseContext(),"saved",Toast.LENGTH_LONG).show();
     }

     catch (Exception e){

         e.printStackTrace();
         Toast.makeText(getBaseContext(),e.toString(),Toast.LENGTH_LONG).show();

     }
    }


   /* private void writeToFile(String data,String file){

        tv = (TextView)findViewById(R.id.textView);

        FileOutputStream outputStream = null;
        try {
            outputStream = openFileOutput(file, MODE_ENABLE_WRITE_AHEAD_LOGGING);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    } */


    private void readFromFile(String file){

        String temp="";
        int c;
        tv = (TextView)findViewById(R.id.textView);
        try {
            FileInputStream fin = openFileInput(file);
            tv.setText("Item last found on :"+temp);
            while( (c = fin.read()) != -1){
                temp = temp + Character.toString((char)c);
            }

            Toast.makeText(getBaseContext(),"file read",Toast.LENGTH_SHORT).show();
        }
        catch(Exception e){
            e.printStackTrace();
        }



       // tv.setText("Item last found on "+temp);
    }

}










