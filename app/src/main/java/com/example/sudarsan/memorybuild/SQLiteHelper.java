package com.example.sudarsan.memorybuild;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.sql.SQLData;

/**
 * Created by SUDARSAN on 25-02-2018.
 */

public class SQLiteHelper extends SQLiteOpenHelper {

    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void queryData(String sql){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(sql);
    }


    public void del(){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL("delete from PROGRESS");
    }

    public void InsertData(String fname, String lname, String age, String phone, byte[] image){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String sql = "INSERT INTO PATIENTS VALUES(NULL,?,?,?,?,?)";

        SQLiteStatement sqLiteStatement = sqLiteDatabase.compileStatement(sql);

        sqLiteStatement.bindString(1,fname);
        sqLiteStatement.bindString(2,lname);
        sqLiteStatement.bindString(3,age);
        sqLiteStatement.bindString(4,phone);
        sqLiteStatement.bindBlob(5,image);

        sqLiteStatement.executeInsert();
    }

    public void insert(String points){


        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String sql = "INSERT INTO PROGRESS VALUES(?)";
        SQLiteStatement sqLiteStatement = sqLiteDatabase.compileStatement(sql);

        sqLiteStatement.bindString(1,points);
        sqLiteStatement.executeInsert();
    }


    public void ins(String place){


        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String sql = "INSERT INTO LOCATION VALUES(?)";
        SQLiteStatement sqLiteStatement = sqLiteDatabase.compileStatement(sql);

        sqLiteStatement.bindString(1,place);
        sqLiteStatement.executeInsert();
    }

    public Cursor getData(String sql){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.rawQuery(sql,null);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
