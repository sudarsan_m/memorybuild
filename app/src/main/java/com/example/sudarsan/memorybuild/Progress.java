package com.example.sudarsan.memorybuild;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class Progress extends AppCompatActivity {

    //SQLiteDatabase db;

    static int i=0,pts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);

        GraphView graph = (GraphView) findViewById(R.id.graph);
        LineGraphSeries<DataPoint> series = null;


       // db = openOrCreateDatabase("PATIENT", Context.MODE_PRIVATE,null);
        // Cursor cursor = db.rawQuery("SELECT * FROM PROGRESS",null);


/*        try {
            graph = (GraphView) findViewById(R.id.graph);
            series = new LineGraphSeries<>(new DataPoint[]{
                    new DataPoint(0, 1),
                    new DataPoint(1, 5),

                    new DataPoint(2, 3)
            });
            graph.addSeries(series);
            DataPoint[] dp = new DataPoint[10];

        }

        catch (Exception e){
            e.printStackTrace();
        }
*/

        try {

            int i = 0,size = 0;
            Cursor cursor = Mydetails.sqLiteHelper.getData("Select count(*) from PROGRESS");
            if(cursor.moveToNext()){
                size = cursor.getInt(0);
            }
            cursor.close();
            cursor = Mydetails.sqLiteHelper.getData("SELECT * FROM PROGRESS");
            DataPoint dp[] = new DataPoint[size];
            while(cursor.moveToNext()){
                double x = new Double(i+1);
                double pts = new Double(cursor.getInt(0));
                dp[i] = new DataPoint(x,pts);
                i++;
            }

            series = new LineGraphSeries<>(dp);
            graph.addSeries(series);

        }

        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }
    }



}
