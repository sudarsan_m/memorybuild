package com.example.sudarsan.memorybuild;

import android.database.Cursor;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import java.util.ArrayList;

public class ContactList extends AppCompatActivity {

    GridView gridView;
    ArrayList<Detail> details;
    List_Adapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        gridView = (GridView)findViewById(R.id.gridView);
        details = new ArrayList<>();
        adapter = new List_Adapter(this,R.layout.list_activity,details);
        gridView.setAdapter(adapter);


        Cursor cursor = Mydetails.sqLiteHelper.getData("SELECT * FROM PATIENTS");
        details.clear();

        while (cursor.moveToNext()){

            int id = cursor.getInt(0);
            String fname = cursor.getString(1);
            String lname = cursor.getString(2);
            String phone = cursor.getString(3);
            String email = cursor.getString(4);
            byte[] image = cursor.getBlob(5);

            details.add(new Detail(id,fname,lname,phone,email,image));
        }

        adapter.notifyDataSetChanged();
    }


}
