package com.example.sudarsan.memorybuild;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity {


    ImageView iv1,iv2,iv3,iv4,iv5;
    TextView tv1,tv2,tv3,tv4,tv5;
    int item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        iv1 = (ImageView)findViewById(R.id.item1);
        iv2 = (ImageView)findViewById(R.id.item2);
        iv3 = (ImageView)findViewById(R.id.item3);
        iv4 = (ImageView)findViewById(R.id.item4);
        iv5 = (ImageView)findViewById(R.id.item5);


        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                item = 1;
                Intent intent = new Intent(Main3Activity.this,Main4Activity.class);
                intent.putExtra("item",item);
                startActivity(intent);
            }
        });


        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                item = 2;
                Intent intent = new Intent(Main3Activity.this,Main4Activity.class);
                intent.putExtra("item",item);
                startActivity(intent);
            }
        });



        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                item = 3;
                Intent intent = new Intent(Main3Activity.this,Main4Activity.class);
                intent.putExtra("item",item);
                startActivity(intent);
            }
        });



        iv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                item = 4;
                Intent intent = new Intent(Main3Activity.this,Main4Activity.class);
                intent.putExtra("item",item);
                startActivity(intent);
            }
        });



        iv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                item = 5;
                Intent intent = new Intent(Main3Activity.this,Main4Activity.class);
                intent.putExtra("item",item);
                startActivity(intent);
            }
        });

    }
}
