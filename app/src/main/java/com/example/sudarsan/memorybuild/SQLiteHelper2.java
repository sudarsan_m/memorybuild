package com.example.sudarsan.memorybuild;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

/**
 * Created by SUDARSAN on 06-03-2018.
 */

public class SQLiteHelper2 extends SQLiteOpenHelper{


    public SQLiteHelper2(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    public void insert(String fname,String lname,String phone,String email,byte[] image){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String sql = "INSERT INTO CONTACT VALUES(NULL,?,?,?,?,?)";

        SQLiteStatement sqLiteStatement = sqLiteDatabase.compileStatement(sql);
        sqLiteStatement.clearBindings();

        sqLiteStatement.bindString(1,fname);
        sqLiteStatement.bindString(2,lname);
        sqLiteStatement.bindString(3,phone);
        sqLiteStatement.bindString(4,email);
        sqLiteStatement.bindBlob(5,image);

        sqLiteStatement.executeInsert();

    }

    public void query(String sql){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(sql);
    }

    public Cursor get(String sql){

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        return sqLiteDatabase.rawQuery(sql,null);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
